from flask import Flask, render_template, request 


app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def page_generator(path):
    a_path = path.split('/')        #getting path name
    end = a_path[-1]                #getting end of path
    if "~" in path or "//" in path or ".." in path:         #forbidden strings in path
        return error_403(403)
    elif end == "name.html":
        return hello(name)
    else:
        return error_404(404)


@app.route('/<name>')
def hello(name):
    request.args.get("name")
    if name == None:
        name = "Annonymous"
    return render_template("name.html", name=name), 200

#Error Handlers
@app.errorhandler(404)
def error_404(error):
    return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(error):
    return render_template("403.html"),403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
